<?php

define('ROOT_PATH', realpath(__DIR__.'/..'));
date_default_timezone_set('America/Buenos_Aires');
error_reporting(E_ALL);

$loader = require_once __DIR__.'/../vendor/autoload.php';

use \SampleWebApp\Helper\DbCreator as DbCreator;

$app = require_once(ROOT_PATH.'/src/app.php');

$em = $app['doctrine.orm.em'];
$userQty = 100;

$dbCreator = new DbCreator($app, 100);

$dbCreator->createUsers();