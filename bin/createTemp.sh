#!/bin/bash

mkdir -p /tmp/user/silexApplication/logs
mkdir -p /tmp/user/silexApplication/twig/cache
mkdir -p /tmp/user/silexApplication/doctrine/proxy
touch /tmp/user/silexApplication/logs/webapp.log
touch /tmp/user/silexApplication/logs/custom.log
touch /tmp/user/silexApplication/logs/monolog.log