<?php

use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\SessionServiceProvider as SessionServiceProvider;
use Silex\Provider\DoctrineServiceProvider as DoctrineServiceProvider;
use Silex\Provider\MonologServiceProvider as MonologProvider;
use Silex\Provider\ValidatorServiceProvider as ValidatorProvider;
use Monolog\Logger as MonologLogger;
use Symfony\Component\HttpFoundation\Request as Request;
use SampleWebApp\Config\DoctrineConfigurator;
use SampleWebApp\Domain\Services\UserServiceImpl as UserServiceImpl;
use SampleWebApp\Domain\Services\AccountServiceImpl as AccountService;
use SampleWebApp\Domain\Services\SecurityService as SecurityService;
use SampleWebApp\Domain\Services\ImperiumImpl as ImperiumSecurityImpl;
use SampleWebApp\Domain\Services\SecurityServiceDummyImp as DummySecurityImpl;
use SampleWebApp\Helper\AclHelper as AclHelper;

/**
 * Setup Log4php
 */
include_once ROOT_PATH . '/vendor/apache/log4php/src/main/php/Logger.php';
\Logger::configure(ROOT_PATH . '/config/log4php.ini');
/**
 * Parse parameters
 * @var [type]
 */
$app = new Application();

$app["debug"] = true;

$config = parse_ini_file(ROOT_PATH . '/config/parameters.ini');
$app['config'] = $config;
$app->register(new UrlGeneratorServiceProvider());
$app->register(new ValidatorServiceProvider());
$app->register(new ServiceControllerServiceProvider());
$app->register(new SessionServiceProvider());

$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => $config['monolog.file'],
    'monolog.level' => $config['monolog.level']
));

$debugTwig = ($app["debug"]===true);
$optimizeTwig = ($app["debug"]===true);
$app->register(new TwigServiceProvider(), array(
    'twig.path' => array(__DIR__ . '/../templates'),
    'twig.options' => array(
        'cache' => $config['twig.cache'],
        'debug' => $debugTwig,
        'strict_variables' => $debugTwig,
        'optimizations'=>($optimizeTwig) ? (1) : (0)
    ),
));
$app->register(new ValidatorProvider());

$app->register(new DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver' => 'pdo_mysql',
        'dbname' => $config['dbName'],
        'host' => $config['dbUrl'],
        'user' => $config['dbUser'],
        'password' => $config['dbPassword'],
        'charset' => 'UTF8'
    )
));

$app['doctrine.orm.em'] = $app->share(function() use ($app) {
    $config = $app["config"];
    $doctrineConfigurator = new DoctrineConfigurator(
        $config["doctrine.proxyDirectory"]
    );
    $em = $doctrineConfigurator->createEm($app['config']);
    return $em;
});

$app['twig'] = $app->share($app->extend('twig', function($twig, $app) {
    // add custom globals, filters, tags, ...
    return $twig;
}));

/**
 * Add helpers
 */
$app["helper.validator"] = $app->share(function() use ($app) {
            $helper = new \SampleWebApp\Helper\ValidatorHelper($app);
            return $helper;
        });

$app["helper.template"] = $app->share(function() use ($app) {
            $em = $app["doctrine.orm.em"];
            $userRepository = $em->getRepository(
                    "SampleWebApp\Domain\Entities\User"
            );
            return new \SampleWebApp\Helper\TemplateHelper($userRepository);
        });
$app["helper.acl"] = $app->share(function() use ($app) {
    return new AclHelper();
});

/**
 * Add services
 * @var UserServiceImpl
 */
$app['service.user'] = $app->share(function() use ($app) {
            $service = new UserServiceImpl($app['doctrine.orm.em']);
            return $service;
        });

$app["service.account"] = $app->share(function() use ($app) {
            $service = new AccountService($app["doctrine.orm.em"]);
            return $service;
        });

$app["service.security"] = $app->share(function() use ($app) {
            $config = $app['config'];
            $service = new ImperiumSecurityImpl(
                $app["doctrine.orm.em"],
                $config['application.id'],
                $config['application.apiKey'],
                $config['api.url']
            );
            $service->setAclHelper($app["helper.acl"]);
            $service->init();
            return $service;
        });



return $app;



