<?php
use SampleWebApp\Domain\Entities\User as UserEntity;
use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException as NotFoundHttpException;
use SampleWebApp\Web\SecurityController as SecurityController;
use SampleWebApp\Web\FrontController as FrontController;
use SampleWebApp\Web\UserController as UserController;
use SampleWebApp\Web\HomeBankController as HomeBankController;
use SampleWebApp\Exception\NotEnoughPrivilege as NotEnoughPrivilegeException;

use RestImperium\Rest\RestClient as RestClient;

$app['controller.security'] = $app->share(function() use ($app) {
            $controller = new SecurityController($app);
            return $controller;
        });
$app['controller.front'] = $app->share(function() use ($app) {
            return new FrontController($app);
        });
$app['controller.user'] = $app->share(function() use ($app) {
            return new UserController($app);
        });
$app["controller.homeBanking"] = $app->share(function() use ($app) {
            return new HomeBankController($app);
        });

/**
 * Match all the request to the static
 * content
 */
/*
  $app->match('/static/{url}', function($url) use ($app) {
  return $app['controller.front']->serveStaticContent($url);
  })->assert('url', '.+')
  ->method('GET');
 */
$beforeRequestCb = function(Request $request) use ($app) {
            $logger = \Logger::getRootLogger();
            $logger->debug("Execute before request");

            $basePath = $request->getBasePath();
            $baseUrl = $request->getBaseUrl();
            $uri = $request->getRequestUri();

            $uri = $request->getRequestUri();
            $explodedUrl = explode("?", $uri);
            $url = $explodedUrl[0];

            $currentUser = $app["service.user"]
                ->getCurrentUser($app["session"]);
            $needsLogin = $app["service.security"]->needsLogin($url);
            if ($needsLogin && ($currentUser===null)) {
                throw new \RuntimeException("User needs to be logged in");
            } else if ($needsLogin===false) {
                return;
            } else if ($needsLogin && ($currentUser!==null)) {
                $information = $app["service.security"]->getAclInformation($url);
                $resource = $information["resource"];
                $action = $information["action"];

                $currentUser = $app["session"]->get("user");

                $role = $currentUser["role"];
                $roleMap = array(
                    UserEntity::ROLE_USER=>"user",
                    UserEntity::ROLE_ADMIN=>"admin",
                    UserEntity::ROLE_ROOT=>"root"
                );
                $roleString = $roleMap[$role];

                $isGranted = $app["service.security"]->isGranted(
                    $roleString,
                    $resource,
                    $action
                );
                if ($isGranted===false) {
                    $message = "Access for the url : %s , (resource,action,role)";
                    $message = $message." (%s,%s,%s) ";
                    $message = sprintf($message, $url, $resource, $action,
                        $roleString);

                    throw new NotEnoughPrivilegeException($message);
                }
            }
        };

$app->get("/", "controller.front:main")->before($beforeRequestCb);

$app->get('/show-login', "controller.security:showLogin")->before($beforeRequestCb);
$app->post('/do-login', "controller.security:doLogin")->before($beforeRequestCb);
$app->get('/do-logout', "controller.security:doLogout")->before($beforeRequestCb);
$app->get('/user/read', "controller.user:showAdminMainPage")->before($beforeRequestCb);

/**
 * Main pages here
 */
$app->get("/ownAccount/read","controller.homeBanking:showOwnAccount")->before($beforeRequestCb);
$app->get("/user/read","controller.user:showAdminMainPage")->before($beforeRequestCb);

/**
 * User create urls
 */
$app->get("/user/show-create", "controller.user:showCreateForm")->before($beforeRequestCb);
$app->post("/user/create","controller.user:handleUserFormSubmit")->before($beforeRequestCb);
/**
 * Remove user url
 */
$app->post("/user/delete-user.json", "controller.user:deleteUser")->before($beforeRequestCb);
/**
 * User edit urls
 */
$app->get("/user/edit", "controller.user:showEditPage")->before($beforeRequestCb);
$app->post("/user/do-edit", "controller.user:doUserEdit")->before($beforeRequestCb);
/**
 * Adjust account url
 */
$app->post("/user/adjust-account.json", "controller.homeBanking:adjustAccount")->before($beforeRequestCb);
$app->get('/account/read', "controller.homeBanking:listAccounts")->before($beforeRequestCb);
$app->get("/account/update", "controller.homeBanking:showEditAccount")->before($beforeRequestCb);

$app->error(function(NotEnoughPrivilegeException $exception, $code) use ($app) {
    $url = "error/notEnoughPrivilege.html";
    $options = array();
    return new Response( $app['twig']->render($url, $options), 500);
});


$logExceptionCb = function($e, $code) use ($app) {
    $logger = \Logger::getLogger('MyLogger');
    $logger = \Logger::getRootLogger();
    $logger->debug(PHP_EOL . "<---------------------------------------->");
    $logger->debug(PHP_EOL . "Exception happened");
    $logger->debug(PHP_EOL . "Code:" . $code);
    $logger->debug(PHP_EOL . "Message:" . $e->getMessage());
    $logger->debug(PHP_EOL . "<---------------------------------------->");
    $logger->debug(PHP_EOL . "Stack trace" . $e->getTraceAsString());
    $logger->debug(PHP_EOL . "<---------------------------------------->");
};

$app->error(function (\Exception $e, $code) use ($app, $logExceptionCb) {
    $logExceptionCb($e, $code);
    if ($app['debug']) {
        return;
    }
    $page = 404 == $code ? '404.html' : '500.html';
    return new Response($app['twig']->render($page, array('code' => $code)), $code);
});

$app->error(function(NotFoundHttpException $e, $code) use (
    $app,
    $logExceptionCb) {
    $logExceptionCb($e, $code);
    return new Response($app['twig']->render('404.html', array('code' => $code)), $code);
});

$app->error(function(\RuntimeException $e, $code) use (
    $app,
    $logExceptionCb) {
    $logExceptionCb($e, $code);
    return new Response($app['twig']->render('500.html', array('code' => $code)), $code);
});