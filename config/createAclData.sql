/*Create the application*/
INSERT INTO im_application (id, apiKey, name, description)
VALUES (100, 'key', 'homeBankingApplication', 'description');

/*
Create the roles
*/
INSERT INTO im_role (id, description, name, application_id)
VALUES (102, 'user', 'user', 100),(103, 'admin', 'admin', 100),
(104, 'root', 'root', 100);

/*
Create the permissions
*/
INSERT INTO im_permission
        (id, resource, action, application_id)
VALUES  (100, "ownAccount", "read", 100),
        (101, "ownAccount", "update", 100);
INSERT INTO im_permission
        (id, resource, action, application_id)
VALUES
        (102,"user","create", 100),
        (103,"user","read", 100),
        (104,"user","update", 100),
        (105,"user","remove", 100);
INSERT INTO im_permission
        (id, resource, action, application_id)
VALUES
        (106,"account", "create", 100),
        (107,"account", "read", 100),
        (108,"account", "update", 100),
        (109,"account", "remove", 100);
/*
Create the subjects
*/
INSERT INTO im_subject (id, name, application_id)
VALUES (100, "subject", 100);
/*
Add the permissions to the roles
*/
INSERT INTO im_permission_role (permission_id, role_id)
VALUES (100,102),(101,102);
INSERT INTO im_permission_role (permission_id, role_id)
VALUES (102,103),(103,103),(104,103),(105,103);
INSERT INTO im_permission_role (role_id, permission_id)
VALUES (104, 100),(104, 101), (104, 102), (104, 103),
(104, 104), (104, 105), (104, 106), (104, 107),
(104, 108), (104, 109);
/*
Add the roles to the subjects
*/
INSERT INTO im_subject_role (subject_id, role_id)
VALUES (100,102),(100,103),(100, 104);