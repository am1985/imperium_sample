<?php
date_default_timezone_set('America/Buenos_Aires');
error_reporting(E_ALL);

define('ROOT_PATH', realpath(__DIR__.'/..'));

$loader = require_once __DIR__.'/../vendor/autoload.php';

$app = require __DIR__.'/../src/app.php';
require __DIR__.'/../config/dev.php';
require __DIR__.'/../src/controllers.php';

$logger = \Logger::getLogger('MyLogger');
$logger->debug('Get an instance of the security service');

$service = $app["service.security"];

/*
Access for user to read the own account is granted
*/
$isGranted = $service->isGranted('user', 'read', 'ownAccount');
$baseMessage = ' The access for the role %s , with permission %s to %s is : %s ';
$stringBoolean = ($isGranted) ? ('GRANTED') : ('NOT GRANTES');
$asString = sprintf($baseMessage, 'user', 'read', 'ownAccount', $stringBoolean);
$logger->debug($asString);
/*
Access for the user to read the account is not granted
*/
$isGranted = $service->isGranted('user', 'read', 'user');
$stringBoolean = ($isGranted) ? ('GRANTED') : ('NOT GRANTES');
$asString = sprintf($baseMessage, 'user', 'read', 'user', $stringBoolean);
$logger->debug($asString);

$logger->debug('End the security test');