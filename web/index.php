<?php
/**
 * Set the error reporting and the time zone
 */
date_default_timezone_set('America/Buenos_Aires');
error_reporting(E_ALL);

define('ROOT_PATH', realpath(__DIR__.'/..'));

$loader = require_once __DIR__.'/../vendor/autoload.php';


/* For production use
$app = require __DIR__.'/../src/app.php';
require __DIR__.'/../config/prod.php';
require __DIR__.'/../src/controllers.php';
$app->run();
*/
/**
 * In debugging mode
 */
include_once("index_dev.php");