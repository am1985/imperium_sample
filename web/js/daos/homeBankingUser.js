define(["jquery"], function() {

    var adjustAccountCb = function(data, successCb, failureCb) {
        var url = "/user/adjust-account.json";

        var aSuccessCb = function(data, textStatus, xmlHttpRequest) {
            successCb(data);
        };

        var aFailureCb = function(xmlHttpRequest, textStatus, errorThrown) {
            failureCb(xmlHttpRequest);
        };

        var options = {
            url : url,
            cache : false,
            type : "POST",
            contentType : "application/json; charset=UTF-8",
            data : JSON.stringify(data),
            success : aSuccessCb,
            failure : aFailureCb
        };

        $.ajax(options);
    };
    return {
        adjustAccount:adjustAccountCb
    };
});