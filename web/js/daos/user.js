define(function (require) {
    var removeUserCb = function(id, successCb, failureCb) {
        var url = "/user/delete-user.json";

        var aSuccessCb = function(data, textStatus, xmlHttpRequest) {
            successCb(data);
        };

        var aFailureCb = function(xmlHttpRequest, textStatus, errorThrown) {
            failureCb(xmlHttpRequest);
        };

        var options = {
            url:url,
            cache:false,
            type:"POST",
            contentType : "application/json; charset=UTF-8",
            data : JSON.stringify({id:id}),
            success : aSuccessCb,
            failure : aFailureCb
        };

        $.ajax(options);

    };


    return {
        removeUser:removeUserCb
    };
});