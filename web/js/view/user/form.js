define(function (require) {
    $ = require('jquery');
    require("jqueryValidate");
    require("jqueryValidateAdditional");
    var loggingModule = require("/js/helper/logger.js");

    var logger = loggingModule.getLogger();

    var module = {
        data:null,
        setData: function() {
            var information = $("#information");
            
            var id = information.attr("data-id");
            id = parseInt(id, 10);
            if (id===NaN) {
                id = -1;
            }

            var name = information.attr("data-name");
            var role = information.attr("data-role");

            module.data = {
                id:id,
                name:name,
                role:role
            };

            /**
             * @todo remove
             */
            logger.debug("The data has been set");
            globalModule = module;
        },
        init:function() {
            /**
             * @todo remove
             */
            console.log("Called init method");
            module.setData();

            if (module.inEdit()===false) {
                module.configureForm();
            } else {
                module.configureFormForAdd();
            }


            if (module.inEdit()===true) {
                module.fillForm();
                module.setupFormForEdition();
            }
        },
        setupFormForEdition : function() {
            var form = $("form");
            form.attr("action", "/user/do-edit");
        },
        fillForm: function() {
            var data = module.data;

            var name = $("input[name='name']");
            name.val(data.name);

            var roleMap = {
                0:"ROLE_USER",
                1:"ROLE_ADMIN",
                2:"ROLE_ROOT"
            };
            var roleString = roleMap[data.role];

            var roleChooser = $("select[name='role']");
            roleChooser.val(roleString);

            roleChooser.attr("disabled", true);
        },
        inEdit: function() {
            return (module.data.id>=0);
        },
        configureFormForAdd: function() {
            var rules = {
                name:{
                    required:true
                }
            };

            var messages = {
                name:{
                    required:"Is required"
                }
            };

            $("form").validate({
                rules:rules,
                messages:messages,
                errorClass:"text-error"
            });

        },
        configureForm:function() {
            var rules = {
                name:{
                    required:true
                },
                password:{
                    required:true,
                    minlength:8
                },
                passwordAgain:{
                    required:true,
                    minlength:8,
                    equalTo:"#password"
                }
            };

            var messages = {
                name:{
                    required:"Is required"
                },
                password:{
                    required:"Is required",
                    minlength:"It must contain at least 8 characters",
                    equalTo:"The passwords do not match"
                }
            };

            $("form").validate({
                rules:rules,
                messages:messages,
                errorClass:"text-error"
            });
        }   
    };

    return {
        init:module.init
    };
});