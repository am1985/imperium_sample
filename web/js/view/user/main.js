define(function (require) {
    $ = require('jquery');
    var loggingModule = require('/js/helper/logger.js');
    var homeBankingDao = require('/js/daos/homeBankingUser.js');

    /**
     * @todo remove
     */
    var logger = loggingModule.getLogger();

    var module = {
        data:null,
        getSubmitValues: function(form) {
            var submitValues = {};

            var values = form.serializeArray();
            var index;
            var eachParameter;
            for (index in values) {
                eachParameter = values[index];
                submitValues[eachParameter.name] = eachParameter.value;
            }
            submitValues["userId"] = module.data.user.id;
            submitValues["accountId"] = module.data.account.id;

            return submitValues;
        },
        setData: function() {
            var information = $("#information");
            var userId = information.attr("data-user-id");
            var accountId = information.attr("data-account-id");

            module.data = {
                user:{
                    id:userId
                },
                account:{
                    id:accountId
                }
            };
        },
        onAccountAdjusted: function(response) {
            var balance = response.balance;

            $("#balance").html(balance);
        },
        onExecuteButton: function(event) {
            var target = $(event.target);
            var form = target.closest("form");
            /*
            var values = form.serializeArray();
            values.user = module.data.user;
            values.account = module.data.account;
            */
            var submitValues = module.getSubmitValues(form);

            homeBankingDao.adjustAccount(submitValues, function(response) {
                module.onAccountAdjusted(response);
            }, function(error) {
            });

        },
        setButtonListeners: function() {
            var me = this;
            $("button[name='execute']").on('click', function(event) {
                event.preventDefault();
                module.onExecuteButton(event);
            });

        },
        init:function() {
            module.setData();
            module.setButtonListeners();

            /**
             * @todo remove
             * @type {[type]}
             */
            globalModule = module;
        }
    };

    return {
        init:module.init
    };

});
