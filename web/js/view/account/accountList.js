define(function(require) {
    var $ = require("jquery");
    require("bootbox");
    var loggingModule = require("/js/helper/logger.js");
    var logger = loggingModule.getLogger();

    var Module = function() {
        this.data = {
        };
    };
    Module.prototype.init = function() {
        this.setListeners();
    };
    Module.prototype.setListeners = function() {
        var me = this;
        $("form").find("button").on("click", function(event) {
            event.preventDefault();
            me.onSearchButton(event);
        });

        $("table tbody").on("tr button").on("click", function(event) {
            var target = $(event.target);
            var action = target.attr("data-action");
            var id = target.attr("data-id");
            event.preventDefault();
            me.onButtonClicked(action, id);
        });
    };
    Module.prototype.onShowEditAccount = function(id) {
        var me = this;
        window.location = "/account/update?id=" + id;
    };
    Module.prototype.onButtonClicked = function(action, id) {
        var validId = id >= 0;
        if (action === "edit" && validId === true) {
            this.onShowEditAccount(id);
        }
    };
    Module.prototype.onSearchButton = function(event) {
        var button = $(event.target);
        var form = button.closest("form");
        var input = form.find("input");

        var location = null;
        if (input === "") {
            //list
            location = "/account/read"
        } else {
            //query
            location = "/account/read?query=" + input.val();
        }
        window.location = location;
    };
    var uniqueInstance = new Module();

    return uniqueInstance;
});