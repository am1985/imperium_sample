define(function (require) {
    var $ = require("jquery");
    //bootbox registers a global object when is loaded
    require("bootbox");

    var loggingModule = require("/js/helper/logger.js");
    var homeBankingDao = require("/js/daos/homeBankingUser.js");
    var userDao = require("/js/daos/user.js");

    //init logger
    var logger = loggingModule.getLogger();


    var module = {
        init:function() {
            module.setListeners();
        },
        onSuccessRemoveUser: function(userData) {
            var rowSelector = "tr[data-id='{id}']";
            rowSelector = rowSelector.replace('{id}', userData.id);
            $("table").find(rowSelector).remove();
        },
        doUserEdit: function(id) {
            var url = "/user/edit?id="+id;
            window.location = url;
        },
        askBeforeRemove: function(id, trueCb, falseCb) {
            bootbox.confirm("Are you sure you want to remove the user?",
                function(response) {
                    if (response===true) {
                        trueCb(id);
                    } else {
                        falseCb();
                    }
                });
        },
        doUserDelete: function(id) {
            userDao.removeUser(id, function(response) {
                var success = response.success;
                if (success===true) {
                    module.onSuccessRemoveUser(response.data);
                }
            }, function(failure) {
                logger.debug("Failure the delete");
            });
        },
        onButtonClicked: function(action, id) {
            if (action==="edit") {
                module.doUserEdit(id);
            } else if (action==="delete") {
                module.askBeforeRemove(id, module.doUserDelete, function() {
                });
            }
        },
        onAddUser: function() {
            logger.debug("On add user clicked");
            window.location = "/user/show-create";
        },
        setListeners: function() {
            $("table tbody").on("tr button").on(
                "click",
                function(event) {
                    var target = $(event.target);
                    var action = target.attr("data-action");
                    var id = target.attr("data-id");

                    module.onButtonClicked(action, id);
                });
            $("#addUserButton").on("click", function(event) {
                var target = $(event.target);
                module.onAddUser();
            });
        }
    };

    return {
        init:module.init
    };
});
