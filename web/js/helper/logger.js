define(function(require) {
    var loggingModule = require('log4javascript');

    var getLoggerCb = function() {
        return log4javascript.getDefaultLogger();
    };

    return {
        getLogger:getLoggerCb
    };
});