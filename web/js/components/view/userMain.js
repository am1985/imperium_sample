define(["jquery","bootstrap","/js/daos/homeBankingUser.js"], function() {
    var fooFunction = function() {
        console.log("The userMain module here");
    };

    var module = {
        onExecuteButton: function(event) {
            var target = $(event.target);
            var form = target.closest("form");
            var values = form.serialize();


        },
        setButtonListeners: function() {
            $("#execute").on('click', function(event) {
                event.preventDefault();

            });

        },
        init:function() {
            module.setButtonListeners();
        }
    };

    return {
        init:module.init
    };
});

/*
    var editApplication = function(data, onSuccessCb, onFailureCb) {
        var aSuccessCb = function(data, textStatus, xmlHttpRequest) {
            onSuccessCb(data);
        };

        var aFailureCb = function(xmlHttpRequest, textStatus, errorThrown) {
            onFailureCb(xmlHttpRequest);
        };

        var options = {
            url : urlManager.getUrlForAppEditSubmit(),
            cache : false,
            type : "POST",
            success : aSuccessCb,
            failure : aFailureCb,
            contentType : "application/json; charset=UTF-8",
            data : JSON.stringify(data)
        };

        $.ajax(options);
    };


 */