require.config({
  paths: {
    "jquery": "/js/components/jquery/jquery",
    "bootstrap":"/js/components/bootstrap/js/bootstrap",
    "log4javascript":"/js/components/logger/log4javascript",
    "bootbox":"/js/components/bootbox/bootbox",
    "jqueryValidate":"/js/components/jQueryValidate/jquery.validate",
    "jqueryValidateAdditional":"/js/components/jQueryValidate/additional-methods"
  },
  shim: {
    "bootstrap":["jquery"],
    "bootbox":["jquery","bootstrap"]
  }
});
