<?php

namespace SampleWebApp\Domain\Entities;

use SampleWebApp\Domain\Entities\HomeBankingUser as HomeBankUser;

/**
 * @Entity(repositoryClass="SampleWebApp\Domain\Repository\AccountRepository")
 * @Table(name="sw_account")
 * @InheritanceType("SINGLE_TABLE")
 * @DiscriminatorColumn(name="discr", type="string")
 * @DiscriminatorMap({"personal" = "PersonalAccount"})
 */
Abstract class AbstractAccount
{

    const ACCOUNT_TYPE_PERSONAL = 0;

    /**
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(name="balance", type="float")
     */
    protected $balance;

    /**
     * @Column(name="maxAmount", type="float")
     */
    protected $limit;

    /**
     * @var SampleWebApp\Domain\Entities\HomeBankingUser
     * @ManyToOne(
     *     targetEntity="HomeBankingUser",
     *     inversedBy="accounts"
     * )
     */
    protected $owner;

    public function __construct($limit)
    {
        $this->balance = 0;
        $this->limit = 0;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getBalance()
    {
        return $this->balance;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setOwner(HomeBankUser $owner)
    {
        $this->owner = $owner;
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public abstract function getType();

    public abstract function debit($amount);

    public abstract function credit($amount);

    public function toArray()
    {
        return array(
            'id' => $this->id,
            'balance' => $this->balance,
            'limit' => $this->limit
        );
    }

    public function __toString()
    {
        $userString = print_r($this->owner, true);

        $answer = "AbstractAccount (id),(owner)";
        $answer .= " (" . $this->id . ")(" . $userString . ")";
        return $answer;
    }

}
