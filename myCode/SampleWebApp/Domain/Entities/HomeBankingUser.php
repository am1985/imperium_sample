<?php

namespace SampleWebApp\Domain\Entities;

use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use SampleWebApp\Domain\Entities\User as User;
use SampleWebApp\Domain\Entities\AbstractAccount as AbstractAccount;

/**
 * @Entity
 */
class HomeBankingUser extends User
{

    /**
     * @var Doctrine\Common\Collections\ArrayCollection
     * @OneToMany(
     *     targetEntity="AbstractAccount",
     *     orphanRemoval=true,
     *     cascade={"persist","remove","merge"},
     *     mappedBy="owner"
     * )
     */
    protected $accounts;

    public function __construct($name, $password, $role)
    {
        parent::__construct($name, $password, $role);
        $this->role = User::ROLE_USER;

        $this->accounts = new ArrayCollection();
    }

    public function addAccount(AbstractAccount $account)
    {
        $account->setOwner($this);
        $this->accounts->add($account);
    }

    /**
     * Returns the personal account or null
     * if cannot be found
     * @return [type] [description]
     */
    public function getPersonalAccount()
    {
        $accounts = $this->accounts;

        $isPersonalAccountCb = function(AbstractAccount $account)
        {
            return (
                $account->getType()===AbstractAccount::ACCOUNT_TYPE_PERSONAL
            );
        };

        $accountsFound = $accounts->filter($isPersonalAccountCb);
        $qty = $accountsFound->count();

        $answer = null;
        if ($qty>=1) {
            $answer = $accountsFound->first();
        }
        return $answer;
    }

    public function __toString()
    {
        return parent::__toString();
    }

    public function toArray()
    {
        $asArray = parent::toArray();
        $accountsArray = $this->accounts->map(function($eachAccount) {
            return $eachAccount->toArray();
        });

        return array_merge($asArray, array(
            'accounts'=>$accountsArray
        ));
    }
}