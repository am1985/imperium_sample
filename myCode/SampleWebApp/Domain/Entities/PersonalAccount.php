<?php

namespace SampleWebApp\Domain\Entities;

use SampleWebApp\Domain\Entities\AbstractAccount as AbstractAccount;

/**
 * @Entity
 */
class PersonalAccount extends AbstractAccount
{
    public function debit($amount)
    {
        $balance = $this->balance;
        $futureBalance = ($balance-$amount);

        if ($futureBalance<0) {
            throw new \RuntimeException("The account has not enough funds");
        } else {
            $this->balance = $futureBalance;
        }
    }

    public function credit($amount)
    {
        $balance = $this->balance;
        $balance += $amount;
        $this->balance = $balance;
    }

    public function getType()
    {
        return AbstractAccount::ACCOUNT_TYPE_PERSONAL;
    }

    public function toArray()
    {
        $asArray = parent::toArray();

        return array_merge($asArray, array(
            'type'=>$this->getType()
        ));
    }

}