<?php
namespace SampleWebApp\Domain\Services;

use SampleWebApp\Domain\Entities\User as UserEntity;
use Symfony\Component\HttpFoundation\Session\SessionInterface as SessionInterface;

interface IUserService
{
    public function findByUserPassword($user, $password);

    public function getCurrentUser(SessionInterface $session);

    public function createHomeBankingUser(array $information);

    public function findById($id);

    public function removeUser($id);

    public function createUSer(UserEntity $user);

    public function updateUser($id, $newInformation);
}
