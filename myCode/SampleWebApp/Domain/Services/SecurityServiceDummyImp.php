<?php

namespace SampleWebApp\Domain\Services;

use SampleWebApp\Domain\Services\AbstractService as AbstractService;
use Symfony\Component\HttpFoundation\Session\SessionInterface as SessionInterface;
use SampleWebApp\Domain\Services\ISecurityService as ISecurityService;
use Doctrine\ORM\EntityManager as EntityManager;
use SampleWebApp\Helper\AclHelper as AclHelper;

class SecurityServiceDummyImp extends AbstractService implements ISecurityService
{

    private $applicationId;
    private $applicationKey;
    /*
    *@var AclHelper
    */
    private $aclHelper;

    private function _getConstructorErrors(EntityManager $em
    ,$applicationId
    ,$applicationKey) {
        $answer = array();

        if ($applicationId===null) {
            $answer['applicationId'] = 'The application id is null';
        } else if ($applicationKey===null) {
            $answer['applicationKey'] = 'The application key is null';
        }

        return $answer;
    }

    public function __construct(EntityManager $em ,$applicationId, $applicationKey)
    {
        $errors = $this->_getConstructorErrors(
            $em,
            $applicationId,
            $applicationKey
        );

        if (count($errors)>=1) {
            throw new \RuntimeException(array_pop($errors));
        }

        parent::__construct($em);
        $this->applicationId = $applicationId;
        $this->applicationKey = $applicationKey;
    }

    private function _getResourceActionMap()
    {
        /**
         * Define acl map
         */
        $aclUser = array(
            "ownAccount" => array("read", "update")
        );
        $aclAdmin = array(
            "user" => array("create", "read", "update", "remove")
        );
        $aclRoot = array(
            "user" => array("create", "read", "update", "remove"),
            "account" => array("create", "read", "update", "remove"),
            "ownAccount"=>array("create", "read", "update", "remove")
        );

        return array(
            "user" => $aclUser,
            "admin" => $aclAdmin,
            "root" => $aclRoot
        );
    }

    public function init()
    {

    }

    public function needsLogin($url)
    {
        $urls = array("/show-login", "/do-login", "/do-logout", "/");
        return (in_array($url, $urls) === false);
    }

    public function isGranted($role, $resource, $action)
    {
        $actionResourceMap = $this->_getResourceActionMap();
        $resources = $actionResourceMap[$role];

        $answer = false;
        if (isset($resources[$resource])===true) {
            $actions = $resources[$resource];
            $answer = in_array($action, $actions);
        }
        return $answer;
    }

    public function setAclHelper(AclHelper $aclHelper)
    {
        $this->aclHelper = $aclHelper;
    }

    public function getAclInformation($url)
    {
        $aclHelper = $this->aclHelper;
        return ($aclHelper->getAclInformation($url));
    }

}