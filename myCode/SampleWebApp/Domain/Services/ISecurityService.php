<?php
namespace SampleWebApp\Domain\Services;

use SampleWebApp\Helper\AclHelper as AclHelper;

interface ISecurityService {
    //put your code here
    public function init();

    public function needsLogin($url);

    public function isGranted($role, $resource, $action);

    public function setAclHelper(AclHelper $aclHelper);
}
?>
