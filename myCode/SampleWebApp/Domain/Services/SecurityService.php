<?php

namespace SampleWebApp\Domain\Services;

use SampleWebApp\Domain\Services\AbstractService as AbstractService;
use Symfony\Component\HttpFoundation\Session\SessionInterface as SessionInterface;
use SampleWebApp\Domain\Services\ISecurityService as ISecurityService;

class SecurityService extends AbstractService implements ISecurityService
{

    public function init()
    {
        
    }

    private function _getResourceMap()
    {
        return array(
            "/ownAccount/read"=>"ownAccount",
            "/user/adjust-account.json"=>"ownAccount",
            "/user/read"=>"user",
            "/user/show-create"=>"user",
            "/user/create"=>"user",
            "/user/delete-user.json"=>"user",
            "/user/edit"=>"user",
            "/user/do-edit"=>"user",
            "/account/read"=>"account",
            "/account/update"=>"account"
        );
    }

    private function _getActionMap()
    {
        return array(
            "/ownAccount/read"=>"read",
            "/user/adjust-account.json"=>"update",
            "/user/read"=>"read",
            "/user/show-create"=>"create",
            "/user/create"=>"create",
            "/user/delete-user.json"=>"remove",
            "/user/edit"=>"update",
            "/user/do-edit"=>"update",
            "/account/read"=>"read",
            "/account/update"=>"update"
        );
    }

    public function needsLogin($url)
    {
        $urls = array("/show-login", "/do-login", "/do-logout", "/");
        return (in_array($url, $urls) === false);
    }

    public function getAclInformation($url)
    {
        $resourceMap = $this->_getResourceMap();
        $actionMap = $this->_getActionMap();

        return array(
            "resource" => $resourceMap[$url],
            "action" => $actionMap[$url]
        );
    }

    private function _getAclMap()
    {
        /**
         * Define acl map
         */
        $aclUser = array(
            "ownAccount" => array("read", "update")
        );
        $aclAdmin = array(
            "user" => array("create", "read", "update", "remove")
        );
        $aclRoot = array(
            "user" => array("create", "read", "update", "remove"),
            "account" => array("create", "read", "update", "remove"),
            "ownAccount"=>array("create", "read", "update", "remove")
        );

        return array(
            "user" => $aclUser,
            "admin" => $aclAdmin,
            "root" => $aclRoot
        );
    }
    
    public function isGranted($role, $resource, $action)
    {
        $aclMap = $this->_getAclMap();
        $mapToUse = $aclMap[$role];
        
        $answer = false;
        $inArray = isset($mapToUse[$resource]);
        if ($inArray) {
            $actionMap = $mapToUse[$resource];
            if (in_array($action, $actionMap)) {
                $answer = true;
            }
        }
        return $answer;
    }

}

?>
