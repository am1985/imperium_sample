<?php
namespace SampleWebApp\Domain\Services;

use SampleWebApp\Domain\Services\AbstractService as AbstractService;
use Symfony\Component\HttpFoundation\Session\SessionInterface as SessionInterface;

use SampleWebApp\Domain\Entities\User as User;
use SampleWebApp\Domain\Entities\HomeBankingUser as HomeBankingUser;
use SampleWebApp\Domain\Entities\PersonalAccount as PersonalAccount;

class UserServiceImpl extends AbstractService implements IUserService
{
    public function createUser(User $user) 
    {
        $em = $this->em;
        $em->persist($user);
    }

    public function findByUserPassword($user, $password)
    {
        $em = $this->em;
        $repository = $em->getRepository('SampleWebApp\Domain\Entities\User');
        $user = $repository->getByNamePassword($user, $password);
        return $user;
    }

    public function getCurrentUser(SessionInterface $session)
    {
        $user = null;
        if ($session!==null) {
            $user = $session->get('user');
        }
        return $user;
    }

    public function createHomeBankingUser(array $information)
    {
        $em = $this->em;

        $name = $information['name'];
        $password = $information['password'];
     
        $user = new HomeBankingUser($name, $password, User::ROLE_USER);

        $personalAccount = new PersonalAccount(30000);
        $user->addAccount($personalAccount);

        $em->persist($user);
        return $user;
    }

    public function findById($id)
    {
        $em = $this->em;
        $repository = $em->getRepository(
            'SampleWebApp\Domain\Entities\User'
        );
        return $repository->findOneBy(array(
            'id'=>$id
        ));
    }

    private function _removeUser(User $user)
    {
        $em = $this->em;
        $em->remove($user);
    }

    private function _removeAdmin(User $user)
    {
        $em = $this->em;
        $em->remove($user);
    }

    private function _removeRoot(User $user)
    {
        $em = $this->em;
        $em->remove($user);
    }

    public function removeUser($id)
    {
        $em = $this->em;
        $repository = $em->getRepository(
            'SampleWebApp\Domain\Entities\User'
        );

        $user = $repository->findOneBy(array(
            'id'=>$id
        ));

        $role = $user->getRole();
        //remove common user
        if ($role===User::ROLE_USER) {
            $this->_removeUser($user);

        //remove admin user
        } else if ($role===User::ROLE_ADMIN) {
            $this->_removeAdmin($user);

        //remove root user
        } else {
            $this->_removeRoot($user);
        }
    }

    public function updateUser($id, $newInformation)
    {
        $em = $this->em;
        $repository = $em->getRepository(
            'SampleWebApp\Domain\Entities\User'
        );

        $name = $newInformation["name"];
        $changePassword = (isset($newInformation["password"]));
        $newPassword = ($changePassword) 
            ? ($newInformation["password"]) : (null);
        
        $user = $repository->findOneBy(array("id"=>$id));

        $user->changeName($name);
        if ($changePassword===true) {
            $user->changePassword($newPassword);
        }

        $em->persist($user);
    }
}