<?php
namespace SampleWebApp\Domain\Services;

use SampleWebApp\Domain\Services\AbstractService as AbstractService;
use Symfony\Component\HttpFoundation\Session\SessionInterface as SessionInterface;

use SampleWebApp\Domain\Entities\User as User;
use SampleWebApp\Domain\Entities\HomeBankingUser as HomeBankingUser;
use SampleWebApp\Domain\Entities\PersonalAccount as PersonalAccount;

class AccountServiceImpl extends AbstractService implements IAccountService
{
    public function findById($id)
    {
        $em = $this->em;
        $repository = $em->getRepository(
            "SampleWebApp\Domain\Entities\AbstractAccount"
        );
        return ($repository->findOneBy(array("id"=>$id)));
    }
}