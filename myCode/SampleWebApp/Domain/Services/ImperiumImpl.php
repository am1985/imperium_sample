<?php

namespace SampleWebApp\Domain\Services;

use SampleWebApp\Domain\Services\AbstractService as AbstractService;
use Symfony\Component\HttpFoundation\Session\SessionInterface as SessionInterface;
use SampleWebApp\Domain\Services\ISecurityService as ISecurityService;
use Doctrine\ORM\EntityManager as EntityManager;
use RestImperium\Sdk\Imperium as ImperiumApiObject;
use SampleWebApp\Helper\AclHelper as AclHelper;

class ImperiumImpl extends AbstractService implements ISecurityService
{

    /*
    *@var int
    */
    private $applicationId;
    /*
    *@var int
    */
    private $applicationKey;
    /*
    *@var ImperiumApiObject
    */
    private $imperiumSdk;
    /*
    *@var AclHelper
    */
    private $aclHelper;
    /**
     * @var string for example "http://imperium.api:300000" 
     */
    private $apiUrl;


    private function _getConstructorErrors(EntityManager $em
    ,$applicationId
    ,$applicationKey
    ,$apiUrl) {
        $answer = array();

        if ($applicationId===null) {
            $answer['applicationId'] = 'The application id is null';
        } else if ($applicationKey===null) {
            $answer['applicationKey'] = 'The application key is null';
        } else if ($apiUrl===null) {
            $answer['apiUrl'] = 'The api url is null';
        }

        return $answer;
    }

    public function __construct(
        EntityManager $em ,
        $applicationId,
        $applicationKey,
        $apiUrl) {
        $errors = $this->_getConstructorErrors(
            $em,
            $applicationId,
            $applicationKey,
            $apiUrl
        );

        if (count($errors)>=1) {
            throw new \RuntimeException(array_pop($errors));
        }

        parent::__construct($em);
        $this->applicationId = $applicationId;
        $this->applicationKey = $applicationKey;
        $this->apiUrl = $apiUrl;
        $this->imperiumSdk = new ImperiumApiObject(array(
          'applicationId'=>$this->applicationId,
          'applicationKey'=>$this->applicationKey,
          'imperiumRestUrl'=>$this->apiUrl
        ));
    }

    private function _subinit()
    {
        $apiObject = $this->imperiumSdk;
        $apiObject->init();
    }

    public function init()
    {
        $this->_subinit();
    }

    public function needsLogin($url)
    {
        $urls = array("/show-login", "/do-login", "/do-logout", "/");
        return (in_array($url, $urls) === false);
    }

    public function isGranted($role, $resource, $action)
    {
        $isGranted = $this->imperiumSdk->isGranted($role, $resource, $action);
        return $isGranted;
    }

    public function setAclHelper(AclHelper $aclHelper)
    {
        $this->aclHelper = $aclHelper;
    }

    public function getAclInformation($url)
    {
        $aclHelper = $this->aclHelper;
        return ($aclHelper->getAclInformation($url));
    }
}