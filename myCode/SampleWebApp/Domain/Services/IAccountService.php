<?php
namespace SampleWebApp\Domain\Services;

use Symfony\Component\HttpFoundation\Session\SessionInterface as SessionInterface;

interface IAccountService
{
    function findById($id);
}