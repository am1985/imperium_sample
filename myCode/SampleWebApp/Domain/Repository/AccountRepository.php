<?php

namespace SampleWebApp\Domain\Repository;

use Doctrine\ORM\EntityRepository as EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator as Paginator;

class AccountRepository extends EntityRepository
{

    private function _getSearchDql()
    {
        return "SELECT account FROM
            SampleWebApp\Domain\Entities\AbstractAccount account
            JOIN account.owner owner WHERE owner.name LIKE :name
            ";
    }

    private function _getDqlList()
    {
        return "
            SELECT account FROM 
            SampleWebApp\Domain\Entities\AbstractAccount account";
    }

    public function listAccounts(array $options)
    {
        $em = $this->_em;

        $offset = $options["offset"];
        $limit = $options["limit"];
        $queryParam = $options["query"];

        $list = ($queryParam === null);

        $dql = null;
        $params = array();
        $query = null;
        if ($list === true) {
            $dql = $this->_getDqlList();
            $query = $em->createQuery($dql);
        } else {
            $dql = $this->_getSearchDql();
            $queryParam = '%'.$queryParam.'%';
            $params = array('name' => $queryParam);
            $query = $em->createQuery($dql);
            $query->setParameters($params);
        }

        $query->setFirstResult($offset);
        $query->setMaxResults($limit);

        return new Paginator($query, true);
    }

}