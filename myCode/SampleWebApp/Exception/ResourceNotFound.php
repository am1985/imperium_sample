<?php

namespace SampleWebApp\Exception;

use SampleWebApp\Exception\ApplicationException as BaseException;

class ResourceNotFound extends BaseException
{

}