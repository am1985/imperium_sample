<?php
namespace SampleWebApp\Helper;

use Silex\Application as Application;
use Doctrine\ORM\EntityManager as EntityManager;
use SampleWebApp\Domain\Entities\User as User;
use SampleWebApp\Domain\Entities\ManagerUser as ManagerUser;
class DbCreator
{
    /**
     *
     * @var Application
     */
    protected $app;

    protected $userQty;

    public function __construct(Application $app, $userQty)
    {
        $this->app = $app;
        $this->userQty = $userQty;
    }

    public function createUsers()
    {
        $app = $this->app;
        $userService = $app['service.user'];
        $em = $app['doctrine.orm.em'];

        $userPrefix = "USER_";
        $userQty = $this->userQty;
        $user = null;

        /**
         * Create User
         */
        for($i=0; $i<$userQty; $i++) {
            $name = 'NAME_USER_'.$i;
            $password = 'password';
            $user = new User($name, $password, User::ROLE_USER);
            //$em->persist($user);
            $userService->createHomeBankingUser(array(
                'name'=>$name,
                'password'=>$password
            ));
        }

        /**
         * Create admins
         */
        for($i=0; $i<$userQty; $i++) {
            $name = 'NAME_ADMIN_'.$i;
            $password = 'password';
            $user = new ManagerUser($name, $password, User::ROLE_ADMIN);
            $em->persist($user);
        }

        /**
         * Create one user , root and admin
         */
        $user = new ManagerUser("root", "password", User::ROLE_ROOT);
        $em->persist($user);
        $user = new ManagerUser("admin", "password", User::ROLE_ADMIN);
        $em->persist($user);

        $em->flush();
    }

}