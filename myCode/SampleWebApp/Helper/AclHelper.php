<?php
namespace SampleWebApp\Helper;

class AclHelper
{
    private $_resourceMap;
    private $_actionMap;

    public function __construct()
    {
        $this->_resourceMap = $this->_getResourceMap();
        $this->_actionMap = $this->_getActionMap();
    }

    private function _getResourceMap()
    {
        return array(
            "/ownAccount/read"=>"ownAccount",
            "/user/adjust-account.json"=>"ownAccount",
            "/user/read"=>"user",
            "/user/show-create"=>"user",
            "/user/create"=>"user",
            "/user/delete-user.json"=>"user",
            "/user/edit"=>"user",
            "/user/do-edit"=>"user",
            "/account/read"=>"account",
            "/account/update"=>"account"
        );
    }

    private function _getActionMap()
    {
        return array(
            "/ownAccount/read"=>"read",
            "/user/adjust-account.json"=>"update",
            "/user/read"=>"read",
            "/user/show-create"=>"create",
            "/user/create"=>"create",
            "/user/delete-user.json"=>"remove",
            "/user/edit"=>"update",
            "/user/do-edit"=>"update",
            "/account/read"=>"read",
            "/account/update"=>"update"
        );
    }

    public function getAclInformation($url)
    {
        $actionMap = $this->_getActionMap();
        $resourceMap = $this->_getResourceMap();

        return array(
            "resource"=>$resourceMap[$url],
            "action"=>$actionMap[$url]
        );
    }
}
