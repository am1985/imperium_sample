<?php
namespace SampleWebApp\Helper;

use Silex\Application as SilexApplication;
use Symfony\Component\Validator\Constraints as Assert;

class ValidatorHelper
{
    /**
     * @var SilexApplication
     */
    private $app;

    public function __construct(SilexApplication $app)
    {
        $this->app = $app;
    }

    public function isValidId($id)
    {
        $app = $this->app;
        $validatorService = $app["validator"];
        $validations = array(
            new Assert\NotBlank(),
            new Assert\Range(array("min"=>0))
        );
        $errors = $validatorService->validateValue($id, $validations);
        return (count($errors)>=1);
    }

}
