<?php

namespace SampleWebApp\Helper;

use SampleWebApp\Domain\Entities\User as UserEntity;
use SampleWebApp\Domain\Repository\UserRepository as UserRepository;

class TemplateHelper {

    protected $userRepository;
    
    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    public function generateUserInfo(UserEntity $user) {
        $answer = $user->toArray();

        $role = $user->getRole();
        $isUser = (strcasecmp($role, UserEntity::ROLE_USER) === 0);
        $isAdmin = (strcasecmp($role, UserEntity::ROLE_ADMIN) === 0);
        $isRoot = (strcasecmp($role, UserEntity::ROLE_ROOT) === 0);

        return array_merge($answer, array(
            "isUser" => $isUser,
            "isAdmin" => $isAdmin,
            "isRoot" => $isRoot
        ));
    }

    public function generateUserInfoFromId($id) {
        $user = $this->userRepository->findOneBy(array("id"=>$id));
        return $this->generateUserInfo($user);
    }

}
