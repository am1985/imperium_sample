<?php

namespace SampleWebApp\Web;

use Silex\Application as Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response as Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\StreamedResponse;
use SampleWebApp\Web\AbstractController as AbstractController;
use SampleWebApp\Exception\ResourceNotFound as ResourceNotFoundException;

class FrontController extends AbstractController
{

    private $container;

    private function _getValidExtensions()
    {
        return array(
            'css',
            'js',
            'png'
        );
    }

    public function main()
    {
        return $this->app->redirect("/show-login");
    }

    /**
     * Receive the relative path to web
     * and return:
     * a css
     * js
     * or image file
     * @param  [type] $content [description]
     * @return [type]          [description]
     */
    public function serveStaticContent($content)
    {
        $fileName = basename($content);

        $explodedFileName = explode(".", $fileName);
        $extension = (count($explodedFileName >= 2)) ? ($explodedFileName[1]) : null;
        $validExtension = in_array($extension, $this->_getValidExtensions());

        if ($validExtension) {
            /*
              return $this->_subserveStaticContent($content, $extension);
             */
            $path = ROOT_PATH . '/web/' . $content;
            $stream = readfile($path);
            $mimeMap = array(
                'png' => 'image/png',
                'js' => 'application/javascript',
                'css' => 'text/css'
            );
            $mimeType = $mimeMap[$extension];
            $response = new Response($stream, 200, array('Content-Type' => $mimeType));
            return $response;
        } else {
            throw new ResourceNotFoundException("For file :" . $content);
        }
    }

}