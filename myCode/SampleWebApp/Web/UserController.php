<?php
namespace SampleWebApp\Web;

use Silex\Application as Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Validator\Constraints as Assert;

use SampleWebApp\Web\AbstractController as AbstractController;
use SampleWebApp\Domain\Services\IUserService as IUserService;
use SampleWebApp\Domain\Entities\User as UserEntity;
use SampleWebApp\Domain\Entities\ManagerUser as ManagerUser;

use \ArrayIterator as ArrayIterator;

class UserController extends AbstractController
{
    private function _showUserMainPage(UserEntity $user)
    {
        $app = $this->app;

        $account = $user->getPersonalAccount();

        $page = "userMain.html";
        $options = array(
            'personalAccount'=>$account->toArray(),
            'user'=>$user
        );
        return (new Response($app['twig']->render($page, $options)));
    }

    private function _showAdminPage(UserEntity $user)
    {
        $app = $this->app;
        return $this->showAdminMainPage();
    }

    private function _showRootPage(UserEntity $user)
    {
        $app = $this->app;
        return $this->showAdminMainPage();
    }

    public function showMainPage()
    {
        $app = $this->app;
        $request = $this->app['request'];

        $roleMap = array(
            UserEntity::ROLE_USER=>'userMain.html',
            UserEntity::ROLE_ADMIN=>'admin/main.html',
            UserEntity::ROLE_ROOT=>'admin/main.html'
        );

        $currentUser = $app['service.user']->getCurrentUser($app["session"]);

        $userInDb = $app['service.user']->findById($currentUser['id']);
        $role = $userInDb->getRole();

        $view = null;
        if ($role===UserEntity::ROLE_USER) {
            $view = $this->_showUserMainPage($userInDb);
        } else if ($role===UserEntity::ROLE_ADMIN) {
            $view = $this->_showAdminPage($userInDb);
        } else if ($role===UserEntity::ROLE_ROOT) {
            $view = $this->_showRootPage($userInDb);
        }

        return $view;
    }

    public function showAdminMainPage()
    {
        $app = $this->app;
        $request = $app['request'];
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 10);

        $userRepository = $app['doctrine.orm.em']->getRepository(
            'SampleWebApp\Domain\Entities\User'
        );

        $paginator = $userRepository->listUsers(array(
            'offset'=>$offset,
            'limit'=>$limit
        ));
        /**
         * User list
         * @var [type]
         */
        $total = count($paginator);
        $iterator = $paginator->getIterator();
        $userArray = array();
        $eachUser = null;
        $asArray = null;
        while($iterator->valid()) {
            $eachUser = $iterator->current();
            $asArray = $eachUser->toArray();
            $asArray['id'] = $eachUser->getId();
            $userArray[] = $asArray;

            $iterator->next();
        }
        /**
         * Pagination options
         */
        $nextOffset = ($offset+$limit);
        $beforeOffset = ($offset-$limit);

        $hasNextPage = ($nextOffset<=$total) && ($nextOffset>=0);
        $hasBeforePage = ($beforeOffset>=1);

        $nextPage = "/user/read?offset=%s&limit=10";
        $beforePage = "/user/read?offset=%s&limit=10";

        $nextOffset = ($hasNextPage) ? ($nextOffset) : (0);
        $beforeOffset = ($hasBeforePage) ? ($beforeOffset) : (0);

        $nextPage = sprintf($nextPage, $nextOffset);
        $beforePage = sprintf($beforePage, $beforeOffset);

        /**
         * [$options description]
         * @var array
         */
        $options = array(
            'users'=>$userArray,
            'hasNextPage'=>$hasNextPage,
            'hasBeforePage'=>$hasBeforePage,
            'nextPage'=>$nextPage,
            'beforePage'=>$beforePage
        );
        
        $currentUser = $app["service.user"]->getCurrentUser($app["session"]);
        $userInfo = $app["helper.template"]->generateUserInfoFromId($currentUser["id"]);
        
        $options = array_merge($userInfo, $options);
        
        return new Response($app['twig']->render(
            "admin/userList.html",
            $options
        ));
    }

    public function deleteUser()
    {
        $app = $this->app;
        $json = json_decode($app["request"]->getContent());
        $userService = $app["service.user"];

        $toDelete = $userService->findById($json->id);
        $asArray = $toDelete->toArray();

        if ($toDelete===null) {
            throw new \RuntimeException("User with id :".$id." not found");
        }

        $userService->removeUser($toDelete);

        return $app->json(array(
            "success"=>true,
            "data"=>$asArray
        ));
    }

    public function showEditPage()
    {
        $app = $this->app;
        $request = $app["request"];
        $id = $request->get("id");
        $userService = $app["service.user"];

        $withErrors = $app["helper.validator"]->isValidId($id);

        if ($withErrors===true) {
            return $app->json(array(
                success=>false,
                message=>'Form with errors'
            ));
        }

        $found = $userService->findById($id);
        if ($found===null) {
            throw new \RuntimeException("User with id :".$id." not found");
        } else {
            return $this->_subShowEditUserForm($id);
        }
    }

    private function _subShowCreateUserForm()
    {
        $app = $this->app;
        $options = array(
            "mode"=>"CREATE",
            "userId"=>'-1',
            "name"=>'-1',
            "role"=>'-1'
        );
        return new Response($app['twig']->render(
            "user/create.html",
            $options
        ));
    }

    private function _subShowEditUserForm($userId)
    {
        $app = $this->app;
        $userService = $app['service.user'];
        $user = $userService->findById($userId);

        $asArray = $user->toArray();
        $options = array(
            "mode"=>"EDIT",
            "userId"=>$asArray["id"],
            "name"=>$asArray["name"],
            "role"=>$asArray["role"]
        );
        return new Response($app['twig']->render(
            "user/create.html",
            $options
        ));
    }

    public function showCreateForm()
    {
        $app = $this->app;
        $request = $app["request"];
        $userService = $app["service.user"];
        $userId = $request->get("userId", -1);

        $currentUser = $userService->getCurrentUser($app["session"]);
        $userInDb = $userService->findById($currentUser["id"]);

        //show create form
        if ($userId===-1) {
            $response = $this->_subShowCreateUserForm();
        //show edit form
        } else {
            $response = $this->_subShowEditUserForm($userId);
        }
        return $response;
    }

    public function handleUserFormSubmit()
    {
        $app = $this->app;
        $request = $app["request"];
        $userService = $app["service.user"];
        $em = $app["doctrine.orm.em"];

        $mode = $request->get("mode", "CREATE");
        $userId = $request->get("userId", -1);
        $name = $request->get("name");
        $password = $request->get("password");
        $passwordAgain = $request->get("passwordAgain");
        $role = $request->get("role", "USER");

        $createUser = (strcasecmp($role, "ROLE_USER")===0);

        $user = null;
        if ($createUser) {
            $information = array(
                "name"=>$name,
                "password"=>$password
            );
            $user = $userService->createHomeBankingUser($information);

        } else {
            $roleMap = array(
                "ROLE_USER"=>UserEntity::ROLE_USER,
                "ROLE_ADMIN"=>UserEntity::ROLE_ADMIN,
                "ROLE_ROOT"=>UserEntity::ROLE_ROOT
            );
            $role = $roleMap[$role];
            $user = new ManagerUser($name, $password, $role);
        }
        $userService->createUser($user);
        $em->flush();
        /*
        return $app->json(array(
            "success"=>true
        ));
        */
        return $this->showAdminMainPage();
    }

    public function doUserEdit() {
        $app = $this->app;
        $request = $app["request"];
        $userService = $app["service.user"];
        $em = $app["doctrine.orm.em"];

        $userId = $request->get("userId", -1);
        $newName = $request->get("name");
        $password = $request->get("password", -1);
        $passwordAgain = $request->get("passwordAgain", -1);

        $user = $userService->findById($userId);

        if ($user===null) {
            throw new \RuntimeException("User with id :".$userId." not found");
        } else {
            $em->flush();
            return $this->_handleUserEdit();
        }
    }

    private function _handleUserEdit()
    {
        $app = $this->app;
        $request = $app["request"];
        $userService = $app["service.user"];
        $em = $app["doctrine.orm.em"];

        $userId = $request->get("userId", -1);
        $newName = $request->get("name");
        $password = $request->get("password", "");
        $passwordAgain = $request->get("passwordAgain", "");

        $user = $userService->findById($userId);

        $information = array(
            "name"=>$newName
        );

        $changePassword = (strlen($password)>=1)
            && (strcasecmp($password, $passwordAgain)===0);
        if ($changePassword===true) {
            $information["password"]=$password;
        }
        $userService->updateUser($user->getId(), $information);

        $em->flush();

        return $this->showMainPage();
    }
 }