<?php

namespace SampleWebApp\Web;

use Silex\Application as Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\StreamedResponse;
use SampleWebApp\Web\AbstractController as AbstractController;
use SampleWebApp\Domain\Services\IUserService as IUserService;
use SampleWebApp\Domain\Entities\User as UserEntity;

class SecurityController extends AbstractController
{

    private function _subShowLoginForm()
    {
        $app = $this->app;
        $page = 'loginForm.html';
        return new Response(
                $app['twig']->render($page, array())
        );
    }

    private function _redirecToMainPage(array $userInformation)
    {
        $app = $this->app;
        $roleString = $userInformation["roleString"];
        $pageMap = array(
            "user"=>"/ownAccount/read?id=%s",
            "admin"=>"/user/read",
            "root"=>"/user/read"
        );
        $url = $pageMap[$roleString];
        
        /**
         * Add own account id for the account
         */
        if (strcasecmp("user", $roleString)===0) {
            $userService = $app["service.user"];
            $user = $userService->findById($userInformation["id"]);
            $accountId = $user->getPersonalAccount()->getId();
            $url = sprintf($url, $accountId);
        }
        
        return $app->redirect($url);
    }

    public function showLogin()
    {
        $request = $this->app['request'];
        $app = $this->app;

        $currentUser = $app['service.user']->getCurrentUser($app['session']);

        if ($currentUser === null) {
            return $this->_subShowLoginForm();
        } else {
            return $this->_redirecToMainPage($currentUser);
        }
    }

    public function doLogin()
    {
        $app = $this->app;
        $request = $app['request'];

        $user = $request->get('user');
        $password = $request->get('password');
        $user = $app['service.user']->findByUserPassword($user, $password);

        if ($user === null) {
            throw new \RuntimeException('User not found');
        } else {
            $session = $app['session'];
            $currentUser = $session->get("user");
            $roleId = $currentUser["role"];
            $roleString = $user->getRoleString();
            
            $userInformation = array(
                'id' => $user->getId(),
                'name' => $user->getName(),
                'role' => $user->getRole(),
                'roleString'=>$roleString
            );

            $session->set('user', $userInformation);
            return $this->_redirecToMainPage($userInformation);
        }
    }

    public function doLogout()
    {
        $app = $this->app;
        $session = $app['session'];

        $currentUser = $app['service.user']->getCurrentUser($session);

        if ($currentUser !== null) {
            $session->invalidate();
        }
        return $app->redirect('/show-login');
    }

}