<?php

namespace SampleWebApp\Web;

use Silex\Application as Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\StreamedResponse;
use SampleWebApp\Web\AbstractController as AbstractController;
use SampleWebApp\Domain\Services\IUserService as IUserService;
use SampleWebApp\Domain\Entities\User as UserEntity;
use \ArrayIterator as ArrayIterator;

class HomeBankController extends AbstractController
{

    public function showOwnAccount()
    {
       return $this->showEditAccount(); 
    }

    public function adjustAccount()
    {
        $app = $this->app;
        $request = $app["request"];
        $accountService = $app["service.account"];
        $em = $app["doctrine.orm.em"];

        $json = json_decode($request->getContent());

        $validOperations = array("DEBIT", "CREDIT");
        $operation = $json->operation;
        $operation = (in_array($operation, $validOperations)) ? ($operation) : ("DEBIT");

        $amount = $json->amount;
        $amount = intval($amount);

        $userId = $json->userId;
        $userId = intval($userId);

        $accountId = $json->accountId;
        $accountId = intval($accountId);

        $accountFound = $accountService->findById($accountId);

        if ($accountFound === null) {
            throw new \RuntimeException(
            "Cannot found account with id: " . $accountId
            );
        }

        $isDebit = (strcasecmp($operation, "DEBIT") === 0);
        if ($isDebit === true) {
            $accountFound->debit($amount);
        } else {
            $accountFound->credit($amount);
        }

        $em->persist($accountFound);
        $em->flush();

        return $app->json($accountFound->toArray(), 200);
    }

    public function showEditAccount()
    {
        $app = $this->app;
        $session = $app["session"];
        $request = $app["request"];
        $id = $request->get("id", -1);
        $repository = $app["doctrine.orm.em"]->getRepository(
                'SampleWebApp\Domain\Entities\AbstractAccount'
        );
        $currentUser = $app["service.user"]->getCurrentUser($session);

        $accountFound = $repository->findOneBy(array("id" => $id));
        if ($accountFound === null) {
            throw new \RuntimeException("Account with id not found");
        } else {

            $options = array(
                'personalAccount' => $accountFound->toArray(),
                'user' => $currentUser
            );
            return new Response($app["twig"]->render("userMain.html", $options));
        }
    }

    public function listAccounts()
    {
        $app = $this->app;
        $request = $app['request'];
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 10);
        $query = $request->get('query', null);

        $repository = $app['doctrine.orm.em']->getRepository(
                'SampleWebApp\Domain\Entities\AbstractAccount'
        );

        $paginator = $repository->listAccounts(array(
            'offset' => $offset,
            'limit' => $limit,
            'query' => $query
        ));

        $total = count($paginator);
        $iterator = $paginator->getIterator();
        $accountArray = array();
        $eachAccount = null;
        $asArray = null;
        $eachOwner = null;
        while ($iterator->valid()) {
            $eachAccount = $iterator->current();
            $eachOwner = $eachAccount->getOwner();

            $asArray = $eachAccount->toArray();
            $asArray['id'] = $eachAccount->getId();
            $asArray['owner'] = $eachOwner->toArray();

            $accountArray[] = $asArray;

            $iterator->next();
        }
        /**
         * Pagination options
         */
        $nextOffset = ($offset + $limit);
        $beforeOffset = ($offset - $limit);

        $hasNextPage = ($nextOffset <= $total) && ($nextOffset >= 0);
        $hasBeforePage = ($beforeOffset >= 1);

        if ($query === null) {
            $nextPage = "/account/list?offset=%s&limit=10";
            $beforePage = "/account/list?offset=%s&limit=10";
            $nextPage = sprintf($nextPage, $nextOffset);
            $beforePage = sprintf($beforePage, $beforeOffset);
        } else {
            $nextPage = "/account/list?offset=%s&limit=10&query=%s";
            $beforePage = "/account/list?offset=%s&limit=10&query=%s";
            $nextPage = sprintf($nextPage, $nextOffset, $query);
            $beforePage = sprintf($beforePage, $beforeOffset, $query);
        }

        $nextOffset = ($hasNextPage) ? ($nextOffset) : (0);
        $beforeOffset = ($hasBeforePage) ? ($beforeOffset) : (0);

        $options = array(
            'accounts' => $accountArray,
            'hasNextPage' => $hasNextPage,
            'hasBeforePage' => $hasBeforePage,
            'nextPage' => $nextPage,
            'beforePage' => $beforePage
        );

        $currentUser = $app["service.user"]->getCurrentUser($app["session"]);
        $userInfo = $app["helper.template"]->generateUserInfoFromId($currentUser["id"]);
        $options = array_merge($userInfo, $options);

        return new Response($app['twig']->render(
                        "accounts/accountList.html", $options
        ));
    }

}